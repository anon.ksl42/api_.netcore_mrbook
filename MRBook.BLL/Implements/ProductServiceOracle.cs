﻿using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using MRBook.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRBook.BLL.Implements
{
    public class ProductServiceOracle : IProductServiceOracle
    {
        private readonly IBaseRepository baseRepository;
        public ProductServiceOracle(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public List<CategoryDto> GetCategory()
        {
            string sql = $@"SELECT CATEID AS CategoryID
                                  ,CATENAME AS CategoryName
                            FROM category";

            return baseRepository.QueryOracle<CategoryDto>(sql).ToList();
        }
    }
}
