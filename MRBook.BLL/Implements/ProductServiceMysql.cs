﻿using MRBook.BLL.Encrypt;
using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using MRBook.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRBook.BLL.Implements
{
    public class ProductServiceMysql : IProductServiceMysql
    {
        private readonly IBaseRepository baseRepository; 
        private const string secretKey = "String to be encrypted";
        public ProductServiceMysql(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public AdminDto DetailAdmin(int id)
        {
            string sql = $@"SELECT AdminID AS AdminID 
                                  ,Username AS Username
                                  ,A_Password AS A_Password
                                  ,Firstname AS Firstname
                                  ,Surname AS Surname
                            FROM administration
                            WHERE AdminID = {id}";
            return baseRepository.QueryMysql<AdminDto>(sql).SingleOrDefault();
        }

        public string EditAdmin(AdminDto acc)
        {
            string sqlResulte = $@"SELECT Username AS Username FROM administration WHERE Username = '{acc.Username}' ";
            var checkUsername = baseRepository.QueryMysql<AdminDto>(sqlResulte).SingleOrDefault();
            if (checkUsername != null)
            {
                return "ชื่อผู้ใช้ซํ้า!";
            }
            else
            {
                string sql = $@"UPDATE administration
                            SET Username = '{acc.Username}',
                                Firstname = '{acc.Firstname}',
                                Surname = '{acc.Surname}'
                            WHERE AdminID = {acc.AdminID}";
                var adminEdit = baseRepository.ExecuteMysql<int>(sql);

                if (adminEdit != 0)
                {
                    return "EditSuccess!";
                }
                else
                {
                    return "UnEditSuccess!";
                }
            }
            

        }

        public List<AdminDto> GetAdmin()
        {
            string sql = $@"SELECT AdminID AS AdminID 
                                  ,Username AS Username
                                  ,A_Password AS A_Password
                                  ,Firstname AS Firstname
                                  ,Surname AS Surname
                            FROM administration";
            return baseRepository.QueryMysql<AdminDto>(sql).ToList();
        }

        public string InsertAdmin(AdminDto acc)
        {
            var password = acc.A_Password;
            var strEncryptred = Secret.Encrypt(secretKey, password);

            string sqlResulte = $@"SELECT Username AS Username FROM administration WHERE Username = '{acc.Username}' ";
            var checkUsername = baseRepository.QueryMysql<AdminDto>(sqlResulte).SingleOrDefault();
            if (checkUsername != null)
            {
                return "ชื่อผู้ใช้ซํ้า!";
            }
            else
            {
                string sql = $@"INSERT INTO administration
                            (Username,A_Password,Firstname,Surname) VALUES ('{acc.Username}','{strEncryptred}','{acc.Firstname}','{acc.Surname}')";

                var query = baseRepository.ExecuteMysql<int>(sql);

                if (query != 0)
                {
                    return "RegisterAdminSuccess";
                }
                else
                {
                    return "UnRegisterAdmin";
                }
            }
            
        }

        public string DeleteAdmin(int id)
        {
            string sql = $@"DELETE FROM administration WHERE AdminID = {id} ";
            var delete = baseRepository.QueryMysql<bool>(sql).SingleOrDefault();
            
            if(delete == false)
            {
                return "DeleteSuccess!";
            }
            else
            {
                return "UnDeleteSuccess!";
            }
        }
    }
}
