﻿using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using MRBook.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRBook.BLL.Implements
{
    public class ProductService : IProductService
    {
        private readonly IBaseRepository baseRepository;

        public ProductService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public int CountOrder()
        {
            string sql = $@"SELECT COUNT(*)
                            FROM [Orders]";
            var a = baseRepository.Query<int>(sql).FirstOrDefault();
            return a;
        }

        public int CountProduct()
        {
            string sql = $@"SELECT COUNT(*)
                            FROM [Product]";
            var a = baseRepository.Query<int>(sql).FirstOrDefault();
            return a;
        }

        
        public List<CategoryDto> GetCategory()
        {
            string sql = $@"SELECT [Category].[CateID] AS CategoryID
                                  ,[Category].[CateName] AS CategoryName
                            FROM [Category]";

           return baseRepository.Query<CategoryDto>(sql).ToList();
        }

        public List<OrderDto> GetOrder()
        {
            string sql = $@"SELECT [Orders].[OrderID] AS OrderID
                                  ,[Orders].[OrderDate] AS OrderDate
                                  ,[Orders].[CustomerID] AS CustomerID
                                  ,[Orders].[ProductID] AS ProductID
                                  ,[Product].[PName] AS ProductName
                                  ,[Customer].[Firstname] AS Firstname
                                  ,[Product].[Price] AS Price
                            FROM [Orders]
                            INNER JOIN [Customer] ON [Orders].[CustomerID] = [Customer].[CusID]
                            INNER JOIN [Product] ON [Orders].[ProductID] = [Product].[P_ID]
                            ORDER BY [Orders].[OrderID] DESC";

            return baseRepository.Query<OrderDto>(sql).ToList();
        }

        public List<OrderDto> GetOrderPage(int page)
        {

            string sql = $@"SELECT [Orders].[OrderID] AS OrderID
                                  ,[Orders].[OrderDate] AS OrderDate
                                  ,[Orders].[CustomerID] AS CustomerID
                                  ,[Orders].[ProductID] AS ProductID
                                  ,[Product].[PName] AS ProductName
                                  ,[Customer].[Firstname] AS Firstname
                                  ,[Product].[Price] AS Price
                            FROM [Orders]
                            INNER JOIN [Customer] ON [Orders].[CustomerID] = [Customer].[CusID]
                            INNER JOIN [Product] ON [Orders].[ProductID] = [Product].[P_ID]
                            ORDER BY [Orders].[OrderID] DESC
                            OFFSET {(page - 1) * 10} ROWS
                            FETCH NEXT {10} ROWS ONLY";

            return baseRepository.Query<OrderDto>(sql).ToList();
        }

        public List<ProductDto> GetProduct(int page)
        {

            string sql = $@"SELECT [Product].[P_ID] AS ProductId
                                  ,[Product].[PName] AS ProductName
                                  ,[Product].[Detail] AS ProductDetail
                                  ,[Product].[Price] AS ProductPrice
                                  ,[Product].[Path_UploadEbook] AS Path_UploadEbook
                                  ,[Product].[Path_ImageCover] AS Path_ImageCover
                                  ,[ProductCategory].[CateID] AS CateID
                                  ,[Category].[CateName] AS CateName
                            FROM [Product]
                            INNER JOIN [ProductCategory] ON [Product].[P_ID] = [ProductCategory].[ProductID]
                            INNER JOIN [Category] ON [ProductCategory].[CateID] = [Category].[CateID]
                            ORDER BY [Product].[P_ID] DESC
                            OFFSET {(page - 1) * 4} ROWS
                            FETCH NEXT {4} ROWS ONLY";

            return baseRepository.Query<ProductDto>(sql).ToList();
        }

        public string InsertOrder(OrderDto model)
        {
            string sql1 = $@"INSERT INTO [Orders]
                            ([OrderDate],[CustomerID],[ProductID])
                            VALUES ('{model.OrderDate}',{model.CustomerID},{model.ProductID})
                            ";
            var result1 = baseRepository.Execute<int>(sql1);

            if (result1 != 0)
            {
                return "BuySuccess";
            }
            else
            {
                return "UnBuySuccess";
            }
        }

        public string InsertProduct(InsertProductDto model)
        {
            string sqlResulte = $@"SELECT [PName] AS ProductName FROM [Product] WHERE [PName] = '{model.ProductName}' ";
            var checkProduct = baseRepository.Query<InsertProductDto>(sqlResulte).SingleOrDefault();

            if (checkProduct != null)
            {
                return "ชื่อหนังสือซํ้า";
            }
            else
            {
                string sql1 = $@"INSERT INTO [Product]
                            ([PName],[Detail],[Price],[Path_UploadEbook],[Path_ImageCover])
                            VALUES ('{model.ProductName}','{model.ProductDetail}',{model.ProductPrice},'{model.Path_UploadEbook}','{model.Path_ImageCover}')
                            ";
                var result1 = baseRepository.Execute<int>(sql1);


                string sql2 = $@"SELECT [P_ID] AS ProductId FROM [Product] WHERE [PName] = '{model.ProductName}' ";

                var result2 = baseRepository.Query<InsertProductDto>(sql2).SingleOrDefault();

                string sql3 = $@"INSERT INTO [ProductCategory]
                            ([ProductID],[CateID])
                            VALUES ({result2.ProductId},{model.CateID})";

                var result3 = baseRepository.Execute<int>(sql3);

                if (result1 != 0 && result3 != 0)
                {
                    return "Successbook";
                }
                else
                {
                    return "Unsuccessbook";
                }
            }

            
        }

        public List<MyBookDto> MyBook(int CusId)
        {
            string sql = $@"SELECT [Orders].[OrderID] AS OrderID
                                  ,[Orders].[CustomerID] AS CustomerID
                                  ,[Orders].[ProductID] AS ProductID
                                  ,[Product].[PName] AS ProductName
                                  ,[Product].[Path_UploadEbook] AS Path_UploadEbook
                                  ,[Product].[Path_ImageCover] AS Path_ImageCover
                            FROM [Orders]
                            INNER JOIN [Customer] ON [Orders].[CustomerID] = [Customer].[CusID]
                            INNER JOIN [Product] ON [Orders].[ProductID] = [Product].[P_ID]
                            WHERE [Orders].[CustomerID] = {CusId}
                            ORDER BY [Orders].[OrderID] DESC";

            return baseRepository.Query<MyBookDto>(sql).ToList();
        }

        public List<ProductCategoryDto> ProductCategory()
        {
            string sql = $@" SELECT [ProductCategory].[ProductID] AS ProductID
                                   ,[ProductCategory].[CateID] AS CateID
                                   ,[Category].[CateName] AS CateName
                                   ,[Product].[PName] AS PName
                                   ,[Product].[Detail] AS Detail
                                   ,[Product].[Price] AS Price
                                   ,[Product].[Path_UploadEbook] AS Path_UploadEbook
                                   ,[Product].[Path_ImageCover] AS Path_ImageCover
                            FROM [ProductCategory] 
                            INNER JOIN [Product] ON [ProductCategory].[ProductID] = [Product].[P_ID]
                            INNER JOIN [Category] ON [ProductCategory].[CateID] = [Category].[CateID]
                            ORDER BY [ProductCategory].[ProductID] DESC";


            return baseRepository.Query<ProductCategoryDto>(sql).ToList();
        }

        public ProductDto ProductDelete(int id)
        {
            string sql1 = $@"DELETE FROM [ProductCategory] WHERE [ProductID]={id}";

            baseRepository.Query<ProductDto>(sql1).SingleOrDefault();

            string sql2 = $@"DELETE FROM [Orders] WHERE [ProductID]={id}";
            baseRepository.Query<ProductDto>(sql1).SingleOrDefault();

            string sql3 = $@"DELETE FROM [Product] WHERE [P_ID]={id}";
            
            return baseRepository.Query<ProductDto>(sql3).SingleOrDefault();
        }

        public ProductDto ProductDetail(int id)
        {

            string sql = $@"SELECT [Product].[P_ID] AS ProductId
                                  ,[Product].[PName] AS ProductName
                                  ,[Product].[Detail] AS ProductDetail
                                  ,[Product].[Price] AS ProductPrice
                                  ,[Product].[Path_UploadEbook] AS Path_UploadEbook
                                  ,[Product].[Path_ImageCover] AS Path_ImageCover
                                  ,[ProductCategory].[CateID] AS CateID
                                  ,[Category].[CateName] AS CateName
                            FROM [Product]
                            INNER JOIN [ProductCategory] ON [Product].[P_ID] = [ProductCategory].[ProductID]
                            INNER JOIN [Category] ON [ProductCategory].[CateID] = [Category].[CateID]
                            WHERE [Product].[P_ID] = {id}";

            return baseRepository.Query<ProductDto>(sql).SingleOrDefault();

        }

        public string ProductEdit(InsertProductDto model)
        {
            
            string sql1 = $@"UPDATE [Product]
                                 SET [PName] = '{model.ProductName}',
                                     [Detail] = '{model.ProductDetail}',
                                     [Price] = {model.ProductPrice} ,
                                     [Path_UploadEbook] = '{model.Path_UploadEbook}',
                                     [Path_ImageCover] = '{model.Path_ImageCover}'
                                 WHERE [P_ID] = {model.ProductId}
                            ";
            var result1 = baseRepository.Execute<int>(sql1);


            string sql2 = $@"UPDATE [ProductCategory]
                                 SET [ProductID] = {model.ProductId},[CateID] = {model.CateID}
                                 WHERE [ProductID] = {model.ProductId}
                                ";

            var result3 = baseRepository.Execute<int>(sql2);

            if (result1 != 0 && result3 != 0)
            {
                return "UpdateSuccessbook";
            }
            else
            {
                return "UnUpdatesuccessbook";
            }
        }

    }
}
