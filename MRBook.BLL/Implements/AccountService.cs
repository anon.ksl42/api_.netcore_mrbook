﻿using MRBook.BLL.Encrypt;
using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using MRBook.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRBook.BLL.Implements
{
    public class AccountService : IAccountService
    {
        private IBaseRepository baseRepository;
        public const string secretKey = "String to be encrypted";
        public AccountService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }


        public bool CheckAccout(string username, string password)
        {
            var strEncryptred = Secret.Encrypt(secretKey, password);

            string sql = $@"SELECT [Administration].[AdminID] 
                            FROM [Administration]
                            WHERE [Administration].[Username] = '{username}' AND [Administration].[A_Password] = '{strEncryptred}' ";
            

            var result = baseRepository.Query<bool>(sql).SingleOrDefault();

            return result;
        }

        public string InsertAdmin(int AdminId, string Username, string Password, string Firstname, string Surname)
        {
            var password = Password;
            var strEncryptred = Secret.Encrypt(secretKey, password);
            

            string sql = $@"INSERT INTO [Administration]
                            ([Username],[A_Password],[Firstname],[Surname]) VALUES ('{Username}','{strEncryptred}','{Firstname}','{Surname}')";

            var query = baseRepository.Execute<int>(sql);

            if (query != 0)
            {
                return "success";
            }
            else
            {
                return "unsuccess";
            }
        }

        public string InsertCustomer(int CusID, string Email, string C_Password, string Firstname, string Surname)
        {
            var password = C_Password;
            var strEncryptred = Secret.Encrypt(secretKey, password);


            string sql = $@"INSERT INTO [Customer]
                            ([Email],[C_Password],[Firstname],[Surname]) VALUES ('{Email}','{strEncryptred}','{Firstname}','{Surname}')";

            var query = baseRepository.Execute<int>(sql);

            if (query != 0)
            {
                return "success";
            }
            else
            {
                return "unsuccess";
            }
        }

        public CustomerDto CheckAccoutCustomer(string Email, string C_Password)
        {
            var strEncryptred = Secret.Encrypt(secretKey, C_Password);

            string sql = $@"SELECT [Customer].[CusID] AS CusID
                                  ,[Customer].[Email] AS Email
                                  ,[Customer].[Firstname] AS Firstname
                                  ,[Customer].[Surname] AS Surname
                            FROM [Customer]
                            WHERE  [Customer].[Email] = '{Email}' AND [Customer].[C_Password] = '{strEncryptred}' ";


            var result = baseRepository.Query<CustomerDto>(sql).FirstOrDefault();

            return result;
        }
    }
}
