﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class AdminDto
    {
        public int AdminID { get; set; }
        public string Username { get; set; }
        public string A_Password { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
