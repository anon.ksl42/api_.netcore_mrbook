﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class CustomerDto
    {
        public int CusID { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
