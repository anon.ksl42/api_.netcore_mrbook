﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class InsertProductDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDetail { get; set; }
        public int ProductPrice { get; set; }
        public string Path_UploadEbook { get; set; }
        public string Path_ImageCover { get; set; }
        public int CateID { get; set; }

    }
}
