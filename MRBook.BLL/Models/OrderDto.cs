﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class OrderDto
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public int Price { get; set; }
        public string Firstname { get; set; }
        public string ProductName { get; set; }

    }
}
