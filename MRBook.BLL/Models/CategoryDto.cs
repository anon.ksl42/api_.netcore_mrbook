﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class CategoryDto
    {

        public int CategoryID { get; set; }
        public string CategoryName { get; set; }

    }
}
