﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Models
{
    public class ProductCategoryDto
    {

        public int ProductID { get; set; }
        public int CateID { get; set; }
        public string CateName { get; set; }
        public string PName { get; set; }
        public string Detail { get; set; }
        public int Price { get; set; }
        public string Path_UploadEbook { get; set; }
        public string Path_ImageCover { get; set; }
    }
}
