﻿using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Interfaces
{
    public interface IProductService
    {
        List<ProductDto> GetProduct(int page);
        string InsertOrder(OrderDto model);
        List<MyBookDto> MyBook(int CusId);
        int CountProduct();
        int CountOrder();
        List<CategoryDto> GetCategory();
        List<ProductCategoryDto> ProductCategory();
        ProductDto ProductDetail(int id);
        string ProductEdit(InsertProductDto model);
        ProductDto ProductDelete(int id);
        string InsertProduct(InsertProductDto model);
        List<OrderDto> GetOrderPage(int page);
        List<OrderDto> GetOrder();
    }
}
