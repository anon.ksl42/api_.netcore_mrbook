﻿using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Interfaces
{
    public interface IProductServiceMysql
    {
        List<AdminDto> GetAdmin();
        AdminDto DetailAdmin(int id);
        string EditAdmin(AdminDto acc);
        string InsertAdmin(AdminDto acc);
        string DeleteAdmin(int id);
    }
}
