﻿using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Interfaces
{
    public interface IAccountService
    {
        bool CheckAccout(string username , string password);
        CustomerDto CheckAccoutCustomer(string Email, string C_Password);
        string InsertAdmin(int AdminId, string Username, string Password,string Firstname, string Surname);
        string InsertCustomer(int CusID, string Email, string C_Password, string Firstname, string Surname);

        
    }
}
