﻿using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.BLL.Interfaces
{
    public interface IProductServiceOracle
    {
        List<CategoryDto> GetCategory();
    }
}
