﻿using API_MRBook.Models;
using Microsoft.AspNetCore.Mvc;
using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Controllers
{
    [Route("api/v1/[Controller]")]
    public class ProductMysqlController : Controller
    {
        private readonly IProductServiceMysql productServiceMysql;
        public ProductMysqlController(IProductServiceMysql productServiceMysql)
        {
            this.productServiceMysql = productServiceMysql;
        }

        [HttpGet("AdminList")]
        public IActionResult GetAdmin()
        {
            try
            {
                var result = productServiceMysql.GetAdmin();
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("DetailAdmin")]
        public IActionResult RegisterAdmin([FromBody] int id)
        {
            try
            {
                var result = productServiceMysql.DetailAdmin(id);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("EditAdmin")]
        public IActionResult AdminEdit([FromBody] AccountModel acc)
        {
            try
            {
                AdminDto model = new AdminDto();
                model.AdminID = acc.AdminId;
                model.Firstname = acc.Firstname;
                model.Surname = acc.Surname;
                model.Username = acc.Username;
                model.A_Password = acc.Password;

                var result = productServiceMysql.EditAdmin(model);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("DeleteAdmin")]
        public IActionResult AdminDelete([FromBody] int id)
        {
            try
            {
                var result = productServiceMysql.DeleteAdmin(id);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("InsertAdmin")]
        public IActionResult RegisterAdmin([FromBody] AccountModel acc)
        {
            try
            {
                AdminDto model = new AdminDto();
                model.AdminID = acc.AdminId;
                model.Firstname = acc.Firstname;
                model.Surname = acc.Surname;
                model.Username = acc.Username;
                model.A_Password = acc.Password;

                var result = productServiceMysql.InsertAdmin(model);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
