﻿using API_MRBook.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MRBook.BLL.Interfaces;
using MRBook.BLL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Controllers
{
    [EnableCors("MyCORSPolicy")]
    [Route("api/v1/[Controller]")]
    public class ProductController : Controller
    {

        private readonly IProductService productService;
        private readonly string UrlImage = "http://localhost:61944/MyImages/";
        private readonly string UrlEbook = "http://localhost:61944/MyEbooks/";
        public ProductController(IProductService productService)
        {
            this.productService = productService;
            
        }

       
        [HttpPost("Book")]
        public IActionResult GetProduct([FromBody] int page)
        {
            try
            {
                var product = productService.GetProduct(page);
                var count = productService.CountProduct();
                var model = product.Select(c => new ProductModel
                {
                    ProductId = c.ProductId,
                    ProductName = c.ProductName,
                    ProductDetail = c.ProductDetail,
                    ProductPrice = c.ProductPrice,
                    Path_ImageCover = c.Path_ImageCover,
                    CateID = c.CateID,
                    CateName = c.CateName

                }).ToList();

                

                return Json(new { count, ListBook = model});
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
        [HttpPost("Order")]
        public IActionResult GetOrder([FromBody] SearchOrPage model)
        {
            try
            {
                
                var result = productService.GetOrderPage(model.page);
                var count = productService.CountOrder();
                var sort = productService.GetOrder().Where(x => x.ProductName.Contains(model.search));
                if (string.IsNullOrEmpty(model.search))
                {
                    return Json(new { order = result, count });
                }
                else
                {
                    return Json(new { order = sort, count });
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("BuyOrder")]
        public IActionResult InsertOrder([FromBody] InsertOrderModel model)
        {
            try
            {
                OrderDto order = new OrderDto();
                order.ProductID = model.ProductID;
                order.OrderDate = model.OrderDate;
                order.CustomerID = model.CustomerID;
                var result = productService.InsertOrder(order);

                if(result == "BuySuccess")
                {
                    return new OkObjectResult(result);
                }
                else
                {
                    return new OkObjectResult(result);
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpGet("Category")]
        public IActionResult GetCategory()
        {
            try
            {
                var categoryType = productService.GetCategory().ToList();

                
                var model = categoryType.Select(c => new CategoryModel
                {
                    CategoryID = c.CategoryID,
                    CategoryName = c.CategoryName
                });
                return Json( new { categoryType = model });

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpGet("ProductCategory")]
        public IActionResult GetProductCategory()
        {
            try
            {
                var productCategory = productService.ProductCategory();


                
                return new OkObjectResult(productCategory);

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("MyBook")]
        public IActionResult GetMyBook([FromBody] int CusId)
        {
            try
            {
                var Mybook = productService.MyBook(CusId);



                return new OkObjectResult(Mybook);

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }


        [HttpPost("InsertBook")]
        public IActionResult InsertProduct([FromForm] InsertProductModel model)
        {
            
            try
            {
               
                if (model.CateID != 0 
                    && model.ProductPrice != 0 
                    && model.ProductName != null 
                    && model.ProductDetail != null 
                    && model.files != null 
                    && model.ebook != null)
                {

                    string pathImage = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Image\" + model.files.FileName);
                    string pathEbook = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Ebook\" + model.ebook.FileName);

                    using (var stream = new FileStream(pathImage, FileMode.Create))
                    {
                        model.files.CopyTo(stream);
                    }
                    using (var stream = new FileStream(pathEbook, FileMode.Create))
                    {
                        model.ebook.CopyTo(stream);
                    }


                    InsertProductDto dataModel = new InsertProductDto();
                    dataModel.Path_ImageCover = UrlImage + model.files.FileName;
                    dataModel.CateID = model.CateID;
                    dataModel.Path_UploadEbook = UrlEbook + model.ebook.FileName;
                    dataModel.ProductDetail = model.ProductDetail;
                    dataModel.ProductName = model.ProductName;
                    dataModel.ProductPrice = model.ProductPrice;

                    var result = productService.InsertProduct(dataModel);

                    if (result == "Successbook")
                    {
                        return new OkObjectResult(result);
                    }
                    else if (result == "ชื่อหนังสือซํ้า")
                    {
                        return new OkObjectResult(result);
                    }
                    else
                    {
                        return new OkObjectResult(BadRequest());
                    }
                }
                else
                {
                    return new OkObjectResult(false); ;
                }

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        
        [HttpPost("EditBook")]

        public IActionResult EdtiProduct([FromForm] InsertProductModel model)
        {

            try
            {

                if (model.CateID != 0 && model.ProductPrice != 0 && model.ProductName != null && model.ProductDetail != null && model.files != null && model.ebook != null)
                {

                    string pathImage = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Image\" + model.files.FileName);
                    string pathEbook = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Ebook\" + model.ebook.FileName);

                    if (model.files.Length > 0 && model.ebook.Length > 0)
                    {
                        using (var stream = new FileStream(pathImage, FileMode.Create))
                        {
                            model.files.CopyTo(stream);
                        }
                        using (var stream = new FileStream(pathEbook, FileMode.Create))
                        {
                            model.ebook.CopyTo(stream);
                        }
                    }


                    InsertProductDto dataModel = new InsertProductDto();
                    dataModel.ProductId = model.ProductId;
                    dataModel.Path_ImageCover = UrlImage + model.files.FileName;
                    dataModel.CateID = model.CateID;
                    dataModel.Path_UploadEbook = UrlEbook + model.ebook.FileName;
                    dataModel.ProductDetail = model.ProductDetail;
                    dataModel.ProductName = model.ProductName;
                    dataModel.ProductPrice = model.ProductPrice;

                    var result = productService.ProductEdit(dataModel);

                    if (result == "UpdateSuccessbook")
                    {
                        return new OkObjectResult(result);
                    }
                    else
                    {
                        return new OkObjectResult(BadRequest());
                    }
                }
                else
                {
                    return new OkObjectResult(false); ;
                }

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("Detail")]
        public IActionResult Detail([FromBody] int ProductId)
        {

            try
            {
                var result = productService.ProductDetail(ProductId);

                return new OkObjectResult(result);
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("Delete")]
        public IActionResult DeleteData([FromBody] int ProductId)
        {

            try
            {
                var result = productService.ProductDelete(ProductId);

                return Json(new {stutus=true,message="Delete Successful." });
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

    }
}
