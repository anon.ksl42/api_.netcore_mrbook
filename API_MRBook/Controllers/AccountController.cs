﻿using API_MRBook.Models;
using Microsoft.AspNetCore.Mvc;
using MRBook.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Controllers
{
    [Route("api/v1/[Controller]")]
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpPost("Login")]
        public IActionResult PostLogin([FromBody] AccountModel model)
        {
            try
            {
                var login = accountService.CheckAccout(model.Username, model.Password);
                if(login == true)
                {
                    return new OkObjectResult(login);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("Register")]
        public IActionResult PostRegister([FromBody] AccountModel model)
        {
            try
            {
                var register = accountService.InsertAdmin(model.AdminId,model.Username,model.Password, model.Firstname, model.Surname);
                if (register == "success")
                {
                    return new OkObjectResult(register);
                }
                else
                {
                    return new OkObjectResult(register);
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
        [HttpPost("RegisterCustomer")]
        public IActionResult RegisterCustomer([FromBody] RegisterCustomerModel model)
        {
            try
            {
                var registerCus = accountService.InsertCustomer(model.CusID, model.Email, model.C_Password, model.Firstname, model.Surname);
                if (registerCus == "success")
                {
                    return new OkObjectResult(registerCus);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost("LoginCustomer")]
        public IActionResult LoginCus([FromBody] RegisterCustomerModel model)
        {
            try
            {
                var login = accountService.CheckAccoutCustomer(model.Email, model.C_Password);
                if (login != null)
                {
                    return Json(new { Status = true, res = login } );
                }
                else
                {
                    return Json(new { Status = false });
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

    }
}
