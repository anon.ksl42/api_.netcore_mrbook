﻿using Microsoft.AspNetCore.Mvc;
using MRBook.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Controllers
{
    [Route("api/v1/[Controller]")]
    public class ProductOracleController : Controller
    {
        private readonly IProductServiceOracle productServiceOracle;
        public ProductOracleController(IProductServiceOracle productServiceOracle)
        {
            this.productServiceOracle = productServiceOracle;
        }

        [HttpGet("CateOracle")]
        public IActionResult GetCategoryMysql()
        {
            try
            {
                var result = productServiceOracle.GetCategory();
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
    }
}
