using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using MRBook.BLL.Implements;
using MRBook.BLL.Interfaces;
using MRBook.DAL.Implements;
using MRBook.DAL.Interfaces;

namespace API_MRBook
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        private IHostingEnvironment CurrentEnvironment { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true).AddEnvironmentVariables();

            CurrentEnvironment = env;
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {   
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDirectoryBrowser();
            services.AddCors(options => {
                options.AddPolicy("MyCORSPolicy", builder => builder
                 .AllowAnyHeader()
                 .AllowAnyOrigin()
                 .AllowAnyMethod());
            });
            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();

            services.AddScoped<IBaseRepository>(_ => new BaseRepository(Configuration.GetConnectionString("DefalutConnectionStringsSqlserver")
                                                                       ,Configuration.GetConnectionString("DefalutConnectionStringsMysql")
                                                                       , Configuration.GetConnectionString("DefalutConnectionStringsOracle")
                                                                       ));
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductServiceMysql, ProductServiceMysql>();
            services.AddScoped<IProductServiceOracle, ProductServiceOracle>();
            services.AddScoped<IAccountService, AccountService>();
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            //http://localhost:61944/MyImages/736462.png 
            //http://localhost:61944/MyEbooks/i-still-cant-speak-english.pdf
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Image")),
                RequestPath = "/MyImages"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Image")),
                RequestPath = "/MyImages"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Ebook")),
                RequestPath = "/MyEbooks"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Ebook")),
                RequestPath = "/MyEbooks"
            });


            app.UseCors("MyCORSPolicy");
            app.UseCookiePolicy();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
