﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Models
{
    public class RegisterCustomerModel
    {
        public int CusID { get; set; }
        public string Email { get; set; }
        public string C_Password { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
