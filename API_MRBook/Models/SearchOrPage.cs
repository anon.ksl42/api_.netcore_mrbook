﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Models
{
    public class SearchOrPage
    {
        public int page { get; set; }
        public string search { get; set; }
    }
}
