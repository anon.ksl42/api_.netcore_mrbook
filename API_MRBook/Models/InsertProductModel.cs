﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Models
{
    public class InsertProductModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDetail { get; set; }
        public int ProductPrice { get; set; }
        public string Path_UploadEbook { get; set; }
        public string Path_UploadAudioBook { get; set; }
        public string Path_ImageCover { get; set; }
        public int CateID { get; set; }
        public IFormFile files { get; set; }
        public IFormFile ebook { get; set; }
    }
}
