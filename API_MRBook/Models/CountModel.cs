﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_MRBook.Models
{
    public class CountModel
    {
        public List<ProductModel> listBook { get; set; }
        public int Count { get; set; }
    }
}
