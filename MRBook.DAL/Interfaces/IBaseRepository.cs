﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRBook.DAL.Interfaces
{
    public interface IBaseRepository
    {
        IEnumerable<T> Query<T>(string sql);
        IEnumerable<T> QueryMysql<T>(string sql);
        IEnumerable<T> QueryOracle<T>(string sql);


        int Execute<T>(string sql);
        int ExecuteMysql<T>(string sql);
        int ExecuteOracle<T>(string sql);

        
    }
}
