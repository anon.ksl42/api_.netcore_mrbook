﻿using Dapper;
using MRBook.DAL.Interfaces;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace MRBook.DAL.Implements
{
    public class BaseRepository : IBaseRepository
    {

        private readonly string ConnectionStrings;
        private readonly string ConnectionStringMysql;
        private readonly string ConnectionStringOracle;
        public BaseRepository(string connectionStrings,string connectionStringMysql,string connectionStringOracle)
        {
            this.ConnectionStrings = connectionStrings;
            this.ConnectionStringMysql = connectionStringMysql;
            this.ConnectionStringOracle = connectionStringOracle;
        }


        private T WithConnectionMysql<T>(Func<IDbConnection, T> getData)
        {
            try
            {
                using (var connection = new MySqlConnection(ConnectionStringMysql))
                {
                    connection.Open();
                    return getData(connection);
                }
            }
            catch (MySqlException e)
            {

                throw e;
            }
        } 
        private T WithConnectionOracle<T>(Func<IDbConnection, T> getData)
        {
            try
            {
                using (var connection = new OracleConnection(ConnectionStringOracle))
                {
                    connection.Open();
                    return getData(connection);
                }
            }
            catch (MySqlException e)
            {

                throw e;
            }
        }

        private T WithConnection<T>(Func<IDbConnection,T> getData)
        {
            try
            {
                using(var connection = new SqlConnection(ConnectionStrings))
                {
                    connection.Open();
                    return getData(connection);
                }
            }
            catch (SqlException e) 
            {

                throw e;
            }
        }
        public IEnumerable<T> Query<T>(string sql)
        {
            return WithConnection(c => c.Query<T>(sql));
        }

        public int Execute<T>(string sql)
        {
            return WithConnection(c => c.Execute(sql));
        }

        public IEnumerable<T> QueryMysql<T>(string sql)
        {
            return WithConnectionMysql(c => c.Query<T>(sql));
        }

        public IEnumerable<T> QueryOracle<T>(string sql)
        {
            return WithConnectionOracle(c => c.Query<T>(sql));
        }

        public int ExecuteMysql<T>(string sql)
        {
            return WithConnectionMysql(c => c.Execute(sql));
        }

        public int ExecuteOracle<T>(string sql)
        {
            return WithConnectionOracle(c => c.Execute(sql));
        }
    }
}
